package goodreads

import (
	"testing"
)

func TestGetReviewOK(t *testing.T) {
	_, err := GetReview("1801051295")
	if err != nil {
		t.Error("Unexpected error when getting a book:" + err.Error())
	}

}

func TestGetReviewInvalidID(t *testing.T) {
	_, err := GetReview("77777777777777777")
	if err == nil {
		t.Error("Unexpected error when getting a book that doesn't exist:" + err.Error())
	}
}
