package goodreads

import "testing"

func TestSearchBookOK(t *testing.T) {
	_, err := SearchBook("End Zone")
	if err != nil {
		t.Error("Unexpected error when searching for books:" + err.Error())
	}
}

func TestSearchStructureOK(t *testing.T) {
	_, err := SearchBook("Lord of the Rings")
	if err != nil {
		t.Error("Unexpected error when searching for books:" + err.Error())
	}
}
