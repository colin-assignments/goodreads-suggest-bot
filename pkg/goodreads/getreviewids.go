package goodreads

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"

	"golang.org/x/net/html"
)

func appendIfMissing(slice []string, id string) []string {
	for _, ele := range slice {
		if ele == id {
			return slice
		}
	}
	return append(slice, id)
}

func getReviewText(t html.Token) (ok bool, text string) {
	// Iterate over all of the Token's attributes until we find a "href"
	for _, a := range t.Attr {
		if a.Key == "href" {
			text = a.Val
			ok = true
		}
	}

	return
}

func parseBody(body io.Reader) []string {
	rev := []string{}
	z := html.NewTokenizer(body)
	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			return rev
		case tt == html.StartTagToken:
			t := z.Token()

			// Extract the gr_review_text value, if there is one
			ok, text := getReviewText(t)
			if !ok {
				continue
			}

			re := regexp.MustCompile(`^https:\/\/www\.goodreads.com\/review\/show\/(\d+)\?utm_campaign=reviews&utm_medium=widget`)
			match := re.FindStringSubmatch(text)

			if len(match) > 0 {
				rev = appendIfMissing(rev, match[1])
			}

		}
	}
}

// GetReviewIDs ..
func GetReviewIDs(isbn string) ([]string, error) {
	reviewIDs := []string{}

	url := APIUri + fmt.Sprintf("/api/reviews_widget_iframe?did=DEVELOPER_ID&amp;format=html&amp;isbn=%s&amp;links=660&amp;min_rating=&amp;review_back=fff&amp;stars=000&amp;text=000", isbn)
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return reviewIDs, err
	}

	if resp.StatusCode != 200 {
		return reviewIDs, errors.New(resp.Status)
	}

	reviewIDs = parseBody(resp.Body)
	return reviewIDs, nil
}
