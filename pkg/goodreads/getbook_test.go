package goodreads

import "testing"

func TestGetBookOK(t *testing.T) {
	_, err := GetBook("631932")
	if err != nil {
		t.Error("Unexpected error when getting a book:" + err.Error())
	}
}

func TestGetBookInvalidID(t *testing.T) {
	_, err := GetBook("77777777777777777")
	if err == nil {
		t.Error("Unexpected error when getting a book that doesn't exist:" + err.Error())
	}
}
