package goodreads

import (
	"encoding/xml"
	"errors"
	"fmt"
	"log"
	"net/http"
)

// GetBook ..
func GetBook(bookID string) (BBook, error) {
	var response BGoodreadsResponse
	var book BBook

	url := APIUri + fmt.Sprintf("/book/show/%s.xml?text_only=true&key=%s", bookID, Key)
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return book, err
	}

	if resp.StatusCode != 200 {
		return book, errors.New(resp.Status)
	}

	if err := xml.NewDecoder(resp.Body).Decode(&response); err != nil {
		log.Println(err)
		return book, err
	}

	return response.BBook, nil
}

// BGoodreadsResponse ..
type BGoodreadsResponse struct {
	XMLName xml.Name `xml:"GoodreadsResponse,omitempty" json:"GoodreadsResponse,omitempty"`
	BBook   BBook    `xml:"book,omitempty" json:"book,omitempty"`
}

// BAverageRating ..
type BAverageRating struct {
	XMLName xml.Name `xml:"average_rating,omitempty" json:"average_rating,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BBestBookID ..
type BBestBookID struct {
	XMLName  xml.Name `xml:"best_book_id,omitempty" json:"best_book_id,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BBook ..
type BBook struct {
	XMLName           xml.Name           `xml:"book,omitempty" json:"book,omitempty"`
	BAuthors          []*BAuthors        `xml:"authors,omitempty" json:"authors,omitempty"`
	BAverageRating    *BAverageRating    `xml:"average_rating,omitempty" json:"average_rating,omitempty"`
	BCountryCode      *BCountryCode      `xml:"country_code,omitempty" json:"country_code,omitempty"`
	BDescription      *BDescription      `xml:"description,omitempty" json:"description,omitempty"`
	BID               *BID               `xml:"id,omitempty" json:"id,omitempty"`
	BImageURL         *BImageURL         `xml:"image_url,omitempty" json:"image_url,omitempty"`
	BISBN             *BISBN             `xml:"isbn,omitempty" json:"isbn,omitempty"`
	BPublicationDay   *BPublicationDay   `xml:"publication_day,omitempty" json:"publication_day,omitempty"`
	BPublicationMonth *BPublicationMonth `xml:"publication_month,omitempty" json:"publication_month,omitempty"`
	BPublicationYear  *BPublicationYear  `xml:"publication_year,omitempty" json:"publication_year,omitempty"`
	BReviewsWidget    *BReviewsWidget    `xml:"reviews_widget,omitempty" json:"reviews_widget,omitempty"`
	BPublisher        *BPublisher        `xml:"publisher,omitempty" json:"publisher,omitempty"`
	BSmallImageURL    *BSmallImageURL    `xml:"small_image_url,omitempty" json:"small_image_url,omitempty"`
	BTitle            *BTitle            `xml:"title,omitempty" json:"title,omitempty"`
	BWork             *BWork             `xml:"work,omitempty" json:"work,omitempty"`
}

// BBooksCount ..
type BBooksCount struct {
	XMLName  xml.Name `xml:"books_count,omitempty" json:"books_count,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BCountryCode ..
type BCountryCode struct {
	XMLName xml.Name `xml:"country_code,omitempty" json:"country_code,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BDefaultChapteringBookID ..
type BDefaultChapteringBookID struct {
	XMLName  xml.Name `xml:"default_chaptering_book_id,omitempty" json:"default_chaptering_book_id,omitempty"`
	Attrnil  string   `xml:"nil,attr"  json:",omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
}

// BDefaultDescriptionLanguageCode ..
type BDefaultDescriptionLanguageCode struct {
	XMLName xml.Name `xml:"default_description_language_code,omitempty" json:"default_description_language_code,omitempty"`
	Attrnil string   `xml:"nil,attr"  json:",omitempty"`
}

// BDescUserID ..
type BDescUserID struct {
	XMLName  xml.Name `xml:"desc_user_id,omitempty" json:"desc_user_id,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BDescription ..
type BDescription struct {
	XMLName xml.Name `xml:"description,omitempty" json:"description,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BID ..
type BID struct {
	XMLName  xml.Name `xml:"id,omitempty" json:"id,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BImageURL ..
type BImageURL struct {
	XMLName xml.Name `xml:"image_url,omitempty" json:"image_url,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BISBN ..
type BISBN struct {
	XMLName xml.Name `xml:"isbn,omitempty" json:"isbn,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BMediaType ..
type BMediaType struct {
	XMLName xml.Name `xml:"media_type,omitempty" json:"media_type,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BOriginalLanguageID ..
type BOriginalLanguageID struct {
	XMLName  xml.Name `xml:"original_language_id,omitempty" json:"original_language_id,omitempty"`
	Attrnil  string   `xml:"nil,attr"  json:",omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
}

// BOriginalPublicationDay ..
type BOriginalPublicationDay struct {
	XMLName  xml.Name `xml:"original_publication_day,omitempty" json:"original_publication_day,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BOriginalPublicationMonth ..
type BOriginalPublicationMonth struct {
	XMLName  xml.Name `xml:"original_publication_month,omitempty" json:"original_publication_month,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BOriginalPublicationYear ..
type BOriginalPublicationYear struct {
	XMLName  xml.Name `xml:"original_publication_year,omitempty" json:"original_publication_year,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BOriginalTitle ..
type BOriginalTitle struct {
	XMLName xml.Name `xml:"original_title,omitempty" json:"original_title,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BPublicationDay ..
type BPublicationDay struct {
	XMLName xml.Name `xml:"publication_day,omitempty" json:"publication_day,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BPublicationMonth ..
type BPublicationMonth struct {
	XMLName xml.Name `xml:"publication_month,omitempty" json:"publication_month,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BPublicationYear ..
type BPublicationYear struct {
	XMLName xml.Name `xml:"publication_year,omitempty" json:"publication_year,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BPublisher ..
type BPublisher struct {
	XMLName xml.Name `xml:"publisher,omitempty" json:"publisher,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BRatingDist ..
type BRatingDist struct {
	XMLName xml.Name `xml:"rating_dist,omitempty" json:"rating_dist,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BRatingsCount ..
type BRatingsCount struct {
	XMLName  xml.Name `xml:"ratings_count,omitempty" json:"ratings_count,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BRatingsSum ..
type BRatingsSum struct {
	XMLName  xml.Name `xml:"ratings_sum,omitempty" json:"ratings_sum,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BReviewsCount ..
type BReviewsCount struct {
	XMLName  xml.Name `xml:"reviews_count,omitempty" json:"reviews_count,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BSmallImageURL ..
type BSmallImageURL struct {
	XMLName xml.Name `xml:"small_image_url,omitempty" json:"small_image_url,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BTextReviewsCount ..
type BTextReviewsCount struct {
	XMLName  xml.Name `xml:"text_reviews_count,omitempty" json:"text_reviews_count,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// BReviewsWidget ..
type BReviewsWidget struct {
	XMLName xml.Name `xml:"reviews_widget,omitempty" json:"reviews_widget,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BTitle ..
type BTitle struct {
	XMLName xml.Name `xml:"title,omitempty" json:"title,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BAuthor ..
type BAuthor struct {
	XMLName xml.Name `xml:"author,omitempty" json:"author,omitempty"`
	BName   *BName   `xml:"name,omitempty" json:"name,omitempty"`
}

// BAuthors ..
type BAuthors struct {
	XMLName xml.Name `xml:"authors,omitempty" json:"authors,omitempty"`
	BAuthor *BAuthor `xml:"author,omitempty" json:"author,omitempty"`
}

// BName ..
type BName struct {
	XMLName xml.Name `xml:"name,omitempty" json:"name,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// BWork ..
type BWork struct {
	XMLName                         xml.Name                         `xml:"work,omitempty" json:"work,omitempty"`
	BBestBookID                     *BBestBookID                     `xml:"best_book_id,omitempty" json:"best_book_id,omitempty"`
	BBooksCount                     *BBooksCount                     `xml:"books_count,omitempty" json:"books_count,omitempty"`
	BDefaultChapteringBookID        *BDefaultChapteringBookID        `xml:"default_chaptering_book_id,omitempty" json:"default_chaptering_book_id,omitempty"`
	BDefaultDescriptionLanguageCode *BDefaultDescriptionLanguageCode `xml:"default_description_language_code,omitempty" json:"default_description_language_code,omitempty"`
	BDescUserID                     *BDescUserID                     `xml:"desc_user_id,omitempty" json:"desc_user_id,omitempty"`
	BID                             *BID                             `xml:"id,omitempty" json:"id,omitempty"`
	BMediaType                      *BMediaType                      `xml:"media_type,omitempty" json:"media_type,omitempty"`
	BOriginalLanguageID             *BOriginalLanguageID             `xml:"original_language_id,omitempty" json:"original_language_id,omitempty"`
	BOriginalPublicationDay         *BOriginalPublicationDay         `xml:"original_publication_day,omitempty" json:"original_publication_day,omitempty"`
	BOriginalPublicationMonth       *BOriginalPublicationMonth       `xml:"original_publication_month,omitempty" json:"original_publication_month,omitempty"`
	BOriginalPublicationYear        *BOriginalPublicationYear        `xml:"original_publication_year,omitempty" json:"original_publication_year,omitempty"`
	BOriginalTitle                  *BOriginalTitle                  `xml:"original_title,omitempty" json:"original_title,omitempty"`
	BRatingDist                     *BRatingDist                     `xml:"rating_dist,omitempty" json:"rating_dist,omitempty"`
	BRatingsCount                   *BRatingsCount                   `xml:"ratings_count,omitempty" json:"ratings_count,omitempty"`
	BRatingsSum                     *BRatingsSum                     `xml:"ratings_sum,omitempty" json:"ratings_sum,omitempty"`
	BReviewsCount                   *BReviewsCount                   `xml:"reviews_count,omitempty" json:"reviews_count,omitempty"`
	BTextReviewsCount               *BTextReviewsCount               `xml:"text_reviews_count,omitempty" json:"text_reviews_count,omitempty"`
}
