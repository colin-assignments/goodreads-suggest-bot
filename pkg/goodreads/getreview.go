package goodreads

import (
	"encoding/xml"
	"errors"
	"fmt"
	"log"
	"net/http"
)

// GetReview ..
func GetReview(reviewID string) (RReview, error) {
	var response RGoodreadsResponse
	var review RReview

	url := APIUri + fmt.Sprintf("/review/show.xml?id=%s&key=%s", reviewID, Key)
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return review, err
	}

	if resp.StatusCode != 200 {
		return review, errors.New(resp.Status)
	}

	if err := xml.NewDecoder(resp.Body).Decode(&response); err != nil {
		log.Println(err)
		return review, err
	}

	return response.RReview, nil
}

// RGoodreadsResponse ..
type RGoodreadsResponse struct {
	XMLName xml.Name `xml:"GoodreadsResponse,omitempty" json:"GoodreadsResponse,omitempty"`
	RReview RReview  `xml:"review,omitempty" json:"review,omitempty"`
}

// RBody ..
type RBody struct {
	XMLName xml.Name `xml:"body,omitempty" json:"body,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RDateAdded ..
type RDateAdded struct {
	XMLName xml.Name `xml:"date_added,omitempty" json:"date_added,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RDateUpdated ..
type RDateUpdated struct {
	XMLName xml.Name `xml:"date_updated,omitempty" json:"date_updated,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RID ..
type RID struct {
	XMLName xml.Name `xml:"id,omitempty" json:"id,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RRating ..
type RRating struct {
	XMLName xml.Name `xml:"rating,omitempty" json:"rating,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RReadAt ..
type RReadAt struct {
	XMLName xml.Name `xml:"read_at,omitempty" json:"read_at,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RReadCount ..
type RReadCount struct {
	XMLName xml.Name `xml:"read_count,omitempty" json:"read_count,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RRecommendedBy ..
type RRecommendedBy struct {
	XMLName xml.Name `xml:"recommended_by,omitempty" json:"recommended_by,omitempty"`
}

// RRecommendedFor ..
type RRecommendedFor struct {
	XMLName xml.Name `xml:"recommended_for,omitempty" json:"recommended_for,omitempty"`
}

// RReview ..
type RReview struct {
	XMLName         xml.Name         `xml:"review,omitempty" json:"review,omitempty"`
	RBody           *RBody           `xml:"body,omitempty" json:"body,omitempty"`
	RDateAdded      *RDateAdded      `xml:"date_added,omitempty" json:"date_added,omitempty"`
	RDateUpdated    *RDateUpdated    `xml:"date_updated,omitempty" json:"date_updated,omitempty"`
	RID             *RID             `xml:"id,omitempty" json:"id,omitempty"`
	RRating         *RRating         `xml:"rating,omitempty" json:"rating,omitempty"`
	RReadAt         *RReadAt         `xml:"read_at,omitempty" json:"read_at,omitempty"`
	RReadCount      *RReadCount      `xml:"read_count,omitempty" json:"read_count,omitempty"`
	RRecommendedBy  *RRecommendedBy  `xml:"recommended_by,omitempty" json:"recommended_by,omitempty"`
	RRecommendedFor *RRecommendedFor `xml:"recommended_for,omitempty" json:"recommended_for,omitempty"`
	RShelves        *RShelves        `xml:"shelves,omitempty" json:"shelves,omitempty"`
	RSpoilerFlag    *RSpoilerFlag    `xml:"spoiler_flag,omitempty" json:"spoiler_flag,omitempty"`
	RSpoilersState  *RSpoilersState  `xml:"spoilers_state,omitempty" json:"spoilers_state,omitempty"`
	RStartedAt      *RStartedAt      `xml:"started_at,omitempty" json:"started_at,omitempty"`
	RVotes          *RVotes          `xml:"votes,omitempty" json:"votes,omitempty"`
}

// RShefl ..
type RShefl struct {
	XMLName       xml.Name `xml:"shelf,omitempty" json:"shelf,omitempty"`
	Attrexclusive string   `xml:"exclusive,attr"  json:",omitempty"`
	Attrid        string   `xml:"id,attr"  json:",omitempty"`
	Attrname      string   `xml:"name,attr"  json:",omitempty"`
	Attrsortable  string   `xml:"sortable,attr"  json:",omitempty"`
}

// RShelves ..
type RShelves struct {
	XMLName xml.Name  `xml:"shelves,omitempty" json:"shelves,omitempty"`
	RShefl  []*RShefl `xml:"shelf,omitempty" json:"shelf,omitempty"`
}

// RSpoilerFlag ..
type RSpoilerFlag struct {
	XMLName xml.Name `xml:"spoiler_flag,omitempty" json:"spoiler_flag,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RSpoilersState ..
type RSpoilersState struct {
	XMLName xml.Name `xml:"spoilers_state,omitempty" json:"spoilers_state,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RStartedAt ..
type RStartedAt struct {
	XMLName xml.Name `xml:"started_at,omitempty" json:"started_at,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// RVotes ..
type RVotes struct {
	XMLName xml.Name `xml:"votes,omitempty" json:"votes,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}
