package goodreads

import (
	"encoding/xml"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

// SearchBook ..
func SearchBook(term string) ([]SWork, error) {
	var response SGoodreadsResponse
	var books []SWork

	url := APIUri + fmt.Sprintf("search/index.xml?search['title']&q=%s&key=%s", url.QueryEscape(term), Key)
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return books, err
	}

	if resp.StatusCode != 200 {
		return books, errors.New(resp.Status)
	}

	if err := xml.NewDecoder(resp.Body).Decode(&response); err != nil {
		log.Println(err)
		return books, err
	}

	return response.SSearch.SResults.SWork, nil
}

// SGoodreadsResponse ..
type SGoodreadsResponse struct {
	XMLName xml.Name `xml:"GoodreadsResponse,omitempty" json:"GoodreadsResponse,omitempty"`
	SSearch *SSearch `xml:"search,omitempty" json:"search,omitempty"`
}

// SAuthor ..
type SAuthor struct {
	XMLName xml.Name `xml:"author,omitempty" json:"author,omitempty"`
	SID     *SID     `xml:"id,omitempty" json:"id,omitempty"`
	SName   *SName   `xml:"name,omitempty" json:"name,omitempty"`
}

// SAverageRating ..
type SAverageRating struct {
	XMLName xml.Name `xml:"average_rating,omitempty" json:"average_rating,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// SBestBook ..
type SBestBook struct {
	XMLName        xml.Name        `xml:"best_book,omitempty" json:"best_book,omitempty"`
	Attrtype       string          `xml:"type,attr"  json:",omitempty"`
	SAuthor        *SAuthor        `xml:"author,omitempty" json:"author,omitempty"`
	SID            *SID            `xml:"id,omitempty" json:"id,omitempty"`
	SImageURL      *SImageURL      `xml:"image_url,omitempty" json:"image_url,omitempty"`
	SSmallImageURL *SSmallImageURL `xml:"small_image_url,omitempty" json:"small_image_url,omitempty"`
	STitle         *STitle         `xml:"title,omitempty" json:"title,omitempty"`
}

// SBooksCount ..
type SBooksCount struct {
	XMLName  xml.Name `xml:"books_count,omitempty" json:"books_count,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// SID ..
type SID struct {
	XMLName  xml.Name `xml:"id,omitempty" json:"id,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// SImageURL ..
type SImageURL struct {
	XMLName xml.Name `xml:"image_url,omitempty" json:"image_url,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// SName ..
type SName struct {
	XMLName xml.Name `xml:"name,omitempty" json:"name,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// SOriginalPublicationDay ..
type SOriginalPublicationDay struct {
	XMLName  xml.Name `xml:"original_publication_day,omitempty" json:"original_publication_day,omitempty"`
	Attrnil  string   `xml:"nil,attr"  json:",omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// SOriginalPublicationMonth ..
type SOriginalPublicationMonth struct {
	XMLName  xml.Name `xml:"original_publication_month,omitempty" json:"original_publication_month,omitempty"`
	Attrnil  string   `xml:"nil,attr"  json:",omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// SOriginalPublicationYear ..
type SOriginalPublicationYear struct {
	XMLName  xml.Name `xml:"original_publication_year,omitempty" json:"original_publication_year,omitempty"`
	Attrnil  string   `xml:"nil,attr"  json:",omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// SRatingsCount ..
type SRatingsCount struct {
	XMLName  xml.Name `xml:"ratings_count,omitempty" json:"ratings_count,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// SResults ..
type SResults struct {
	XMLName xml.Name `xml:"results,omitempty" json:"results,omitempty"`
	SWork   []SWork  `xml:"work,omitempty" json:"work,omitempty"`
}

// SSearch ..
type SSearch struct {
	XMLName  xml.Name `xml:"search,omitempty" json:"search,omitempty"`
	SResults SResults `xml:"results,omitempty" json:"results,omitempty"`
}

// SSmallImageURL ..
type SSmallImageURL struct {
	XMLName xml.Name `xml:"small_image_url,omitempty" json:"small_image_url,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// STextReviewsCount ..
type STextReviewsCount struct {
	XMLName  xml.Name `xml:"text_reviews_count,omitempty" json:"text_reviews_count,omitempty"`
	Attrtype string   `xml:"type,attr"  json:",omitempty"`
	Text     string   `xml:",chardata" json:",omitempty"`
}

// STitle ..
type STitle struct {
	XMLName xml.Name `xml:"title,omitempty" json:"title,omitempty"`
	Text    string   `xml:",chardata" json:",omitempty"`
}

// SWork ..
type SWork struct {
	XMLName                   xml.Name                   `xml:"work,omitempty" json:"work,omitempty"`
	SAverageRating            *SAverageRating            `xml:"average_rating,omitempty" json:"average_rating,omitempty"`
	SBestBook                 *SBestBook                 `xml:"best_book,omitempty" json:"best_book,omitempty"`
	SBooksCount               *SBooksCount               `xml:"books_count,omitempty" json:"books_count,omitempty"`
	SID                       *SID                       `xml:"id,omitempty" json:"id,omitempty"`
	SOriginalPublicationDay   *SOriginalPublicationDay   `xml:"original_publication_day,omitempty" json:"original_publication_day,omitempty"`
	SOriginalPublicationMonth *SOriginalPublicationMonth `xml:"original_publication_month,omitempty" json:"original_publication_month,omitempty"`
	SOriginalPublicationYear  *SOriginalPublicationYear  `xml:"original_publication_year,omitempty" json:"original_publication_year,omitempty"`
	SRatingsCount             *SRatingsCount             `xml:"ratings_count,omitempty" json:"ratings_count,omitempty"`
	STextReviewsCount         *STextReviewsCount         `xml:"text_reviews_count,omitempty" json:"text_reviews_count,omitempty"`
}
