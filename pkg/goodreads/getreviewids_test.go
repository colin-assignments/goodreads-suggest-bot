package goodreads

import "testing"

func TestGetReviewsOK(t *testing.T) {
	_, err := GetReviewIDs("231262")
	if err != nil {
		t.Error("Unexpected error when searching for books:" + err.Error())
	}
}
