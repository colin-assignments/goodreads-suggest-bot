package toneanalyzer

import (
	"testing"
)

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func TestToneOK(t *testing.T) {
	text := `The cover of this book is what caught my eye, what a beautiful illustration. There are whimsical drawings throughout the book and a wonderful gallery of photographs of Ms. Montgomery with some of her animal friends at the end of the book.`
	_, err := GetTone(text, nil)
	if err != nil {
		t.Error("Unexpected error for Tone: " + err.Error())
		return
	}
}

func TestTonePositiveOK(t *testing.T) {
	text := `For a debut novel this is astoundingly good, and it has the feel of a classic novel. It expertly moves between two timelines, events that occurred in 1986 to the present. Set in the small town of Anderbury, 4 friends, Eddie 'Munster' Adams, Fat Gav, Metal Mickey, Hoppo and Nicky are 12 years old, whiling away the summer holidays getting into mischief on their bikes, having fun, evolving secret communications through chalk men symbols and in search of excitement and adventure. `
	tones, err := GetTone(text, nil)
	if err != nil {
		t.Error("Unexpected error for Tone: " + err.Error())
		return
	}

	tonesDetected := 0
	positiveTones := []string{"joy", "confident"}
	for _, tone := range tones.DocumentTone.Tones {
		if contains(positiveTones, tone.ToneID) {
			tonesDetected++
		}
	}

	if tonesDetected == 0 {
		t.Error("Unexpected error for Positive Tone: ")
		return
	}
}

func TestToneNegativeOK(t *testing.T) {
	text := `I started this book, but I just can’t bring myself to finish it. Feel free to message me and tell me how it ends. I just can’t take it anymore. `
	tones, err := GetTone(text, nil)
	if err != nil {
		t.Error("Unexpected error for Tone: " + err.Error())
		return
	}

	tonesDetected := 0
	negativeTones := []string{"anger", "fear", "sadness"}
	for _, tone := range tones.DocumentTone.Tones {
		if contains(negativeTones, tone.ToneID) {
			tonesDetected++
		}
	}

	if tonesDetected == 0 {
		t.Error("Unexpected error for Negative Tone: ")
		return
	}
}
