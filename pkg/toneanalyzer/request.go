package toneanalyzer

import (
	"errors"
	"io"
	"io/ioutil"
	"net/http"
)

func requestWatsonTonePOST(url string, body io.Reader, header http.Header) ([]byte, error) {
	client := &http.Client{}
	req, err := http.NewRequest("POST", url, body)

	req.SetBasicAuth(userName, password)
	for key := range header {
		req.Header.Set(key, header[key][0])
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	return responseBody, nil
}
