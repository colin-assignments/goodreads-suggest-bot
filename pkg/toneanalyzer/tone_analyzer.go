package toneanalyzer

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

// Analysis ..
type Analysis struct {
	DocumentTone  DocumentAnalysis   `json:"document_tone"`
	SentencesTone []SentenceAnalysis `json:"sentences_tone"`
}

// DocumentAnalysis ..
type DocumentAnalysis struct {
	Tones []ToneScore `json:"tones"`
}

// ToneScore ..
type ToneScore struct {
	ToneName string  `json:"tone_name"`
	ToneID   string  `json:"tone_id"`
	Score    float64 `json:"score"`
}

// SentenceAnalysis ..
type SentenceAnalysis struct {
	SentenceID int         `json:"sentence_id"`
	InputFrom  int         `json:"input_from"`
	InputTo    int         `json:"input_to"`
	Text       string      `json:"text"`
	Tones      []ToneScore `json:"tones"`
}

// GetTone ..
func GetTone(text string, options map[string]interface{}) (Analysis, error) {
	q := url.Values{}
	for k, v := range options {
		q.Set(k, fmt.Sprintf("%v", v))
	}
	q.Set("version", defaultMinorVersion)

	headers := make(http.Header)
	headers.Set("Content-Type", "text/plain")
	headers.Set("Accept", "application/json")

	url := defaultURL + fmt.Sprintf("/%s/tone?%s", defaultMajorVersion, q.Encode())
	fmt.Println(url)

	body, err := requestWatsonTonePOST(url, strings.NewReader(text), headers)
	if err != nil {
		return Analysis{}, err
	}

	var analysis Analysis
	err = json.Unmarshal(body, &analysis)
	return analysis, err
}
