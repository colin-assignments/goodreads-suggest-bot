FROM alpine:3.4

MAINTAINER Colin Bennett
ARG APP_NAME
ARG VERSION
ARG COMMIT

ENV APP_NAME=${APP_NAME:-bin/service-goodreads-suggest-bot}
ENV VERSION=${VERSION:-development}
ENV COMMIT=${COMMIT}

RUN apk update
RUN apk add ca-certificates

COPY $APP_NAME /bin/app

EXPOSE 8080

ENTRYPOINT ["/bin/app"]

CMD ["/"]
