.PHONY: test-all ##@test Test the Goodreads Suggest Bot Packages
test-all: test-goodreads test-toneanalyzer

.PHONY: test-goodreads
test-goodreads: ##@test Test the Goodreads Package
	@echo "Testing the Goodreads Package"
	@go test -cover ./pkg/goodreads
	@echo "Goodreads Package test completed"

.PHONY: test-toneanalyzer
test-toneanalyzer: ##@test Test the Watson Tone Analyzer Package
	@echo "Testing the Watson Tone Analyzer Package"
	@go test -cover ./pkg/toneanalyzer
	@echo "Watson Tone Analyzer Package test completed"