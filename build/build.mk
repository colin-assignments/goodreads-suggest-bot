.PHONY: build-goodreads-suggest-bot
build-goodreads-suggest-bot: ##@build Run the build for the Goodreads Suggest Bot Service
	@echo "Compiling the Goodreads Suggest Bot Service"
	@go build -o ${CI_PROJECT_DIR}/bin/service-goodreads-suggest-bot ./services/goodreads
	@echo "Goodreads Suggest Bot Service compiled: ${CI_PROJECT_DIR}/bin/service-goodreads-suggest-bot"
