.PHONY: docker-goodreads-suggest-bot
docker-goodreads-suggest-bot: ##@docker Run the build for the Goodreads Suggest Bot Service Docker Image
	@echo "Building Goodreads Suggest Bot Service Docker Image"
	@docker build -f ./Dockerfile -t ${PRIVATE_REGISTRY}/goodreads-suggest-bot:${CI_BUILD_TAG} --build-arg APP_NAME="bin/service-goodreads-suggest-bot"  .