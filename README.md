# Web Developer Application Challenge

Goodreads Suggest Bot - Created in GO

## Problem

Help readers evaluate books using facebook messenger chatbot.

## Tasks

- Welcome the user by using their first name.
- Ask the user if they want to search books by name or by ID (Goodreads ID).
- Use Goodreads API to search books.
- Retrieve a maximum of 5 books and let the user select one of them. 
- Suggest the user if they should buy the book or not based on the semantic analysis done in the previous step.
- Test coverage should be 60% or more.
- Provisioning script to the repo.

## Instructions

The bot is availible at [DropseekerHQ](https://www.facebook.com/messages/t/dropseekerHQ)
The Facebook Page is one I had created in the past.

`https://www.facebook.com/messages/t/dropseekerHQ`

## Building

I have created a Makefile to simplify the compiling of the app, running tests and building the Docker image.
eg. `make build-goodreads-suggest-bot` will compile the app. 

*NOTE:*  `export GOOS?=linux` with this variable set the app will only compile on Linux.

## Deployment

The application is deployed on Herokus Container Engine.
I've wrote a simple script called `deploy.sh` that takes care of the deployment to Heroku.