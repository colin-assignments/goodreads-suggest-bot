#!/bin/bash
# Deploy container to a Heroku.
# You need to specify $HEROKU_API_KEY secret and $HEROKU_APP_NAME in environment before using this script.

# Adjust project naming for Heroku
# Set manually in this demo, but would be in .evn file
PROJECT_NAME="mindvalley"
HEROKU_API_KEY="ce88c818-1737-4557-980f-0bddb1072718"
HEROKU_APP_NAME="goodreads-messenger-bot"

echo "Logging in into Heroku"
docker login --username=_ --password=${HEROKU_API_KEY} registry.heroku.com

echo "Building application"
make build-goodreads-suggest-bot

echo "Pushing container to a Heroku CE"
heroku container:push web --app ${HEROKU_APP_NAME}

echo "Release container one Heroku CE"
heroku container:release web --app ${HEROKU_APP_NAME}