package main

// FacebookEvent ..
type FacebookEvent struct {
	Object string `json:"object,omitempty"`
	Entry  []struct {
		ID        string      `json:"id,omitempty"`
		Time      int         `json:"time,omitempty"`
		Messaging []Messaging `json:"messaging,omitempty"`
	} `json:"entry,omitempty"`
}

// Messaging ..
type Messaging struct {
	Sender    User     `json:"sender,omitempty"`
	Recipient User     `json:"recipient,omitempty"`
	Timestamp int      `json:"timestamp,omitempty"`
	Message   Message  `json:"message,omitempty"`
	Postback  Postback `json:"postback,omitempty"`
}

// User ..
type User struct {
	ID string `json:"id,omitempty"`
}

// Message ..
type Message struct {
	MID        string `json:"mid,omitempty"`
	Text       string `json:"text,omitempty"`
	QuickReply *struct {
		Payload string `json:"payload,omitempty"`
	} `json:"quick_reply,omitempty"`
	Attachment *Attachment `json:"attachment,omitempty"`
}

// Postback ..
type Postback struct {
	Title    string `json:"title,omitempty"`
	Payload  string `json:"payload,omitempty"`
	Referral struct {
		Ref    string `json:"ref,omitempty"`
		Source string `json:"source,omitempty"`
		Type   string `json:"type,omitempty"`
	} `json:"referral,omitempty"`
}

// Attachment ..
type Attachment struct {
	Type    string  `json:"type,omitempty"`
	Payload Payload `json:"payload,omitempty"`
}

// Response ..
type Response struct {
	Recipient User    `json:"recipient,omitempty"`
	Message   Message `json:"message,omitempty"`
}

// Payload ..
type Payload struct {
	URL          string    `json:"url,omitempty"`
	TemplateType string    `json:"template_type,omitempty"`
	Elements     []Element `json:"elements,omitempty"`
}

// Element ..
type Element struct {
	Title    string    `json:"title,omitempty"`
	Subtitle string    `json:"subtitle,omitempty"`
	ImageURL string    `json:"image_url,omitempty"`
	Buttons  []Buttons `json:"buttons,omitempty"`
}

// Buttons ..
type Buttons struct {
	Type    string `json:"type,omitempty"`
	Title   string `json:"title,omitempty"`
	Payload string `json:"payload,omitempty"`
}
