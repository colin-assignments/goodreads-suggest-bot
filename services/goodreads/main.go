package main

import (
	"flag"
	"log"
	"os"
)

var (
	hostFlag         *string
	portFlag         *int
	versionFlag      *bool
	versionShortFlag *bool

	port                    string
	verifyFacebookToken     string
	accessFacebookPageToken string
)

const (
	serviceName    = "Goodreads Bot"
	facebookAPIURL = "https://graph.facebook.com/v2.11/me/messages?access_token=%s"
)

func init() {
	hostFlag = flag.String("host", "0.0.0.0", "The host address to listen on.")
	portFlag = flag.Int("port", 8080, "The port to listen on.")
	versionFlag = flag.Bool("version", false, "Prints the version of this application.")
	versionShortFlag = flag.Bool("v", false, "Prints the version of this application.")

	port = os.Getenv("PORT")
	verifyFacebookToken = "VpkUiA18qVlT6EsKFk3bSPxBmzuVxF9L"
	accessFacebookPageToken = "EAADFes8lUOsBAAEgYySm68neyYkgGRYG8GluENpVwwnP8sXBSYZBZBFw4P5mnxta3qMYi7EKlLkmqgcZBDQhksOvkZBkT3KA81S4XlBLMsSNgeXtZBUUiEkPY5eZCyQbe41YJhZBZBsGSOG3CN6hlMVZAx2oGZCy4BsvBceBorijXIFwZDZD"

}

// This init function will print the version and if the version flag is set exit
func init() {
	flag.Parse()
	log.Println(serviceName)

	if *versionFlag || *versionShortFlag {
		os.Exit(0)
	}
}

// This init function will attempt to connect to the Facebook Messenger API
func init() {
}

func main() {
	log.Println("Starting")
	r := router()

	if port == "" {
		port = "8080"
	}

	if err := r.Run(":" + port); err != nil {
		log.Fatal(err)
	}
}
