package main

import (
	"bytes"
	"log"

	"gitlab.com/colin-assignments/goodreads-suggest-bot/pkg/goodreads"
	"gitlab.com/colin-assignments/goodreads-suggest-bot/pkg/toneanalyzer"
)

func toneAnalysis(reviews []goodreads.RReview) (float64, error) {
	var buffer bytes.Buffer
	for _, review := range reviews {
		buffer.WriteString(review.RBody.Text)

	}

	result, err := toneanalyzer.GetTone(buffer.String(), nil)
	if err != nil {
		log.Println(err)
	}

	totalScore := 0.0
	for _, tone := range result.DocumentTone.Tones {
		if tone.Score > 0 {

			switch tone.ToneID {
			case "joy", "confident":
				totalScore += tone.Score
				log.Println("Positive tone detected")
			case "anger", "fear", "sadness":
				totalScore += 1 - tone.Score
				log.Println("Negative tone detected")
			}
		}
	}

	return totalScore, nil
}
