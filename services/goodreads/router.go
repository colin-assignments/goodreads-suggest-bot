package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func router() *gin.Engine {
	engine := gin.New()
	engine.Use(
		gin.Logger(),
		gin.Recovery(),
	)

	engine.GET("/", func(c *gin.Context) {
		c.Data(http.StatusOK, "text/plain", []byte(`OK`))
	})

	engine.GET("/webhook", getValidation)
	engine.POST("/webhook", handleMessage)
	return engine
}

// This method
func getValidation(c *gin.Context) {
	challenge := c.Request.URL.Query().Get("hub.challenge")
	mode := c.Request.URL.Query().Get("hub.mode")
	token := c.Request.URL.Query().Get("hub.verify_token")

	if mode != "" && token == verifyFacebookToken {
		c.Data(http.StatusOK, "text/plain", []byte(challenge))
	} else {
		c.Data(http.StatusForbidden, "text/plain", []byte(`Forbidden`))
	}
}
