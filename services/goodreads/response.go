package main

func generateResponseStruct(senderID, message string) Response {
	return Response{
		Recipient: User{
			ID: senderID,
		},
		Message: Message{
			Text: message,
		},
	}
}
