package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
)

func requestFacebookPOST(response Response) error {
	client := http.Client{}
	url := fmt.Sprintf(facebookAPIURL, accessFacebookPageToken)

	body := new(bytes.Buffer)
	json.NewEncoder(body).Encode(&response)

	req, err := http.NewRequest("POST", url, body)
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	return nil
}
