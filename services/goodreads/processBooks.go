package main

import (
	"fmt"
	"log"

	"gitlab.com/colin-assignments/goodreads-suggest-bot/pkg/goodreads"
)

func getBook(id string) (goodreads.BBook, error) {
	book, err := goodreads.GetBook(id)
	if err != nil {
		log.Println(err)
		return goodreads.BBook{}, err
	}

	return book, nil
}

func getBookElement(id string) ([]Element, error) {
	var elements []Element
	book, err := goodreads.GetBook(id)
	if err != nil {
		log.Println(err)
		return elements, err
	}

	author := book.BAuthors[0].BAuthor.BName.Text
	return []Element{
		{
			Title:    fmt.Sprintf("%s, by %s", book.BTitle.Text, author),
			Subtitle: book.BDescription.Text,
			ImageURL: book.BImageURL.Text,
			Buttons: []Buttons{
				{
					Type:    "postback",
					Title:   "Choose This Book",
					Payload: book.BID.Text,
				},
			},
		},
	}, nil
}

func getBookElements(searchTerm string) ([]Element, error) {
	var elements []Element
	books, err := goodreads.SearchBook(searchTerm)
	if err != nil {
		log.Println(err)
		return elements, err
	}

	for i, book := range books {
		if i == 5 {
			break
		}

		element := Element{
			Title:    fmt.Sprintf("%s, by %s", book.SBestBook.STitle.Text, book.SBestBook.SAuthor.SName.Text),
			ImageURL: book.SBestBook.SImageURL.Text,
			Buttons: []Buttons{
				{
					Type:    "postback",
					Title:   "Choose This Book",
					Payload: book.SBestBook.SID.Text,
				},
			},
		}

		elements = append(elements, element)
	}

	return elements, nil
}

func getBookReviews(isbn string) ([]goodreads.RReview, error) {
	var reviews []goodreads.RReview
	reviewIDs, err := goodreads.GetReviewIDs(isbn)
	if err != nil {
		log.Println(err)
		return reviews, err
	}

	for _, id := range reviewIDs {
		review, err := goodreads.GetReview(string(id))
		if err != nil {
			log.Println(err)
		}

		reviews = append(reviews, review)
	}

	return reviews, nil
}
