package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/huandu/facebook"
)

const (
	greetingText = "Welcome to Goodreads recommendation service. Please type the Title or Goodreads ID of a book."
	analysisText = "Goodreads is analysing the book you selected."
	notFoundText = "Goodreads could not find any book. Please try again."
	foundText    = "Goodreads found the following books."
	approveText  = "Goodreads recommends the book: "
	rejectText   = "Goodreads does not recommend the book: "
	isbnText     = "Goodreads could not analyse the book as there is no ISBN on record."
)

func processMessage(event Messaging) {
	payload := event.Message.Text

	elements, err := getBookElement(payload)
	if err != nil {
		log.Println(err)
	}

	if len(elements) == 0 {
		elements, err = getBookElements(payload)
		if err != nil {
			log.Println(err)
		}

		if len(elements) == 0 {
			notFoundResponse := generateResponseStruct(event.Sender.ID, notFoundText)
			err = requestFacebookPOST(notFoundResponse)
			if err != nil {
				panic(err)
			}
			return
		}

	}

	foundResponse := generateResponseStruct(event.Sender.ID, foundText)
	err = requestFacebookPOST(foundResponse)
	if err != nil {
		panic(err)
	}

	elementResponse := generateResponseStruct(event.Sender.ID, "")
	elementResponse.Message.Attachment = &Attachment{
		Type: "template",
		Payload: Payload{
			TemplateType: "generic",
			Elements:     elements,
		},
	}

	err = requestFacebookPOST(elementResponse)
	if err != nil {
		panic(err)
	}

	return
}

func handleBookSelection(senderID, bookID string) {
	book, err := getBook(bookID)
	if err != nil {
		panic(err)
	}

	isbn := book.BISBN.Text
	if isbn == "" {
		isbnResponse := generateResponseStruct(senderID, isbnText)

		err = requestFacebookPOST(isbnResponse)
		if err != nil {
			panic(err)
		}
		return

	}

	analysisResponse := generateResponseStruct(senderID, analysisText)

	err = requestFacebookPOST(analysisResponse)
	if err != nil {
		panic(err)
	}

	reviews, err := getBookReviews(isbn)
	if err != nil {
		panic(err)
	}

	total, err := toneAnalysis(reviews)
	if err != nil {
		panic(err)
	}

	if total >= 0.5 {
		selectionResponse := generateResponseStruct(senderID, approveText+fmt.Sprintf("'%s'", book.BTitle.Text))

		err = requestFacebookPOST(selectionResponse)
		if err != nil {
			panic(err)
		}
		return
	}

	selectionResponse := generateResponseStruct(senderID, rejectText+fmt.Sprintf("'%s'", book.BTitle.Text))

	err = requestFacebookPOST(selectionResponse)
	if err != nil {
		panic(err)
	}

	return

}

func handleGreetingMessage(senderID string) {
	res, _ := facebook.Get(senderID, facebook.Params{
		"fields":       "first_name",
		"access_token": accessFacebookPageToken,
	})

	helloResponse := generateResponseStruct(senderID, fmt.Sprintf("Hello %s", res["first_name"]))
	err := requestFacebookPOST(helloResponse)
	if err != nil {
		panic(err)
	}

	greetingResponse := generateResponseStruct(senderID, greetingText)
	err = requestFacebookPOST(greetingResponse)
	if err != nil {
		panic(err)
	}

	return
}

func processPostback(event Messaging) {
	payload := event.Postback.Payload
	if payload == "GET_STARTED" {
		handleGreetingMessage(event.Sender.ID)
		return
	}

	handleBookSelection(event.Sender.ID, payload)
	return
}

func handleMessage(c *gin.Context) {
	decoder := json.NewDecoder(c.Request.Body)
	var facebookEvent FacebookEvent

	err := decoder.Decode(&facebookEvent)
	if err != nil {
		panic(err)
	}

	if facebookEvent.Object == "page" {
		for _, entry := range facebookEvent.Entry {
			message := entry.Messaging[0]

			if message.Message != (Message{}) {
				processMessage(message)
			} else if message.Postback != (Postback{}) {
				processPostback(message)
			} else {
				log.Println("Unknown message type")
			}
		}

		c.Data(http.StatusOK, "text/plain", []byte(`Message recieved`))

	} else {
		c.Data(http.StatusNotFound, "text/plain", []byte(`Message not supported`))

	}
}
